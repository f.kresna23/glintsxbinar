const axios = require("axios");

const fetchApi = async () => {
  let urlCryptoAtm = "https://coinmap.org/api/v1/venues/";
  let urlExchangeRate = "https://api.coingecko.com/api/v3/exchange_rates";
  let urlCryptoData = "https://api.coinpaprika.com/v1/coins/btc-bitcoin";
  let data = {};

  try {
    const response = await Promise.all([
      axios.get(urlCryptoAtm),
      axios.get(urlExchangeRate),
      axios.get(urlCryptoData),
    ]);

    data = {
      cryptoAtm: response[0].data,
      exchangeRate: response[1].data,
      cryptoData: response[2].data,
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
