const axios = require("axios");

let urlCryptoAtm = "https://coinmap.org/api/v1/venues/";
let urlExchangeRate = "https://api.coingecko.com/api/v3/exchange_rates";
let urlCryptoData = "https://api.coinpaprika.com/v1/coins/btc-bitcoin";
let data = {};

axios
  .get(urlCryptoAtm)
  .then((response) => {
    data = { cryptoAtm: response.data };

    return axios.get(urlExchangeRate);
  })
  .then((response) => {
    data = { ...data, exchangeRate: response.data };

    return axios.get(urlCryptoData);
  })
  .then((response) => {
    data = { ...data, cryptoData: response.data };

    console.log(data);
  })
  .catch((err) => {
    console.log(err.message);
  });
