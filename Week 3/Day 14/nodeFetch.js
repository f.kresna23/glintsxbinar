import fetch from "node-fetch";

const nodeFetch = async () => {
  let urlCryptoAtm = "https://coinmap.org/api/v1/venues/";
  let urlExchangeRate = "https://api.coingecko.com/api/v3/exchange_rates";
  let urlCryptoData = "https://api.coinpaprika.com/v1/coins/btc-bitcoin";
  let data = {};
  try {
    //Async Await
    // const response1 = await fetch(urlCryptoAtm);
    // const response2 = await fetch(urlExchangeRate);
    // const response3 = await fetch(urlCryptoData);

    // const result1 = await response1.json();
    // const result2 = await response2.json();
    // const result2 = await response3.json();

    // const crpytoAtm = result1;
    // const exchangeRate = result2;
    // const cryptoData = result3;

    // console.log(crpytoAtm);
    // console.log(exchangeRate);
    // console.log(cryptoData);

    // Promise
    const cryptoCurrency = await Promise.all([
      fetch(urlCryptoAtm)
        .then((res) => res.json())
        .then((res) => res),
      fetch(urlExchangeRate)
        .then((res) => res.json())
        .then((res) => res),
      fetch(urlCryptoData)
        .then((res) => res.json())
        .then((res) => res),
    ]);

    data = {
      crpytoAtm: cryptoCurrency[0],
      exchangeRate: cryptoCurrency[1],
      cryptoData: cryptoCurrency[2],
    };
    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

nodeFetch();
