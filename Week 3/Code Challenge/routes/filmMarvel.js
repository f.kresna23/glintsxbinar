const express = require("express"); // Import express

// Import Students Controller
const {
  getAllFilmMarvel,
  getDetailFilm,
  addFilm,
  updateFilm,
  deleteFilm,
} = require("../controllers/filmMarvel");

const router = express.Router(); // Import router

router.get("/", getAllFilmMarvel);

router.get("/:id", getDetailFilm);

router.post("/", addFilm);

router.put("/:id", updateFilm);

router.delete("/:id", deleteFilm);

module.exports = router; // Exports router
