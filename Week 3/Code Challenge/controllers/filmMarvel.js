let data = require("../models/data.json"); // Ambil data dari json

// Membuat Film Marvel class
class FilmMarvel {
  getAllFilmMarvel(req, res, next) {
    try {
      // It will response to client with status 200 (OK) and { data: data }
      res.status(200).json({ data: data });
    } catch (error) {
      // If something error, it will return response with status 500 and { errors: ["Internal Server Error"] }
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  getDetailFilm(req, res, next) {
    try {
      // It will filter data by req.params.id
      const detailData = data.filter(
        (item) => item.id === parseInt(req.params.id)
      );

      // If no data exists
      if (detailData.length === 0) {
        // It will response client with status 404 and { errors: ["Film gak ketemu nih, belum release mungkin ya"] }
        return res
          .status(404)
          .json({ errors: ["Film gak ketemu nih, belum release mungkin ya"] });
      }

      // It will response to client with status 200 (OK) and { data: data }
      res.status(200).json({ data: detailData });
    } catch (error) {
      // If something error, it will return response with status 500 and { errors: ["Internal Server Error"] }
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  addFilm(req, res, next) {
    try {
      // Get the last id of students
      let lastId = data[data.length - 1].id;

      // Add data student to data
      data.push({
        id: lastId + 1,
        judul: req.body.judul,
        produser: req.body.produser,
        sutradara: req.body.sutradara,
        scooring: req.body.scooring,
        durasi: req.body.durasi,
      });

      // It will response to client with status 201 (Created) and { data: data }
      res.status(201).json({ data: data });
    } catch (error) {
      // If something error, it will return response with status 500 and { errors: ["Internal Server Error"] }
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  updateFilm(req, res, next) {
    try {
      // Find the data is exist or not
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      // If data not exists
      if (!findData) {
        // It will response client with status 404 and { errors: ["Film gak ketemu nih, belum release mungkin ya"] }
        return res
          .status(404)
          .json({ errors: ["Film gak ketemu nih, belum release mungkin ya"] });
      }

      // Update data student to data by req.params.id
      data = data.map((item) =>
        item.id === parseInt(req.params.id)
          ? {
              id: parseInt(req.params.id),
              judul: req.body.judul,
              produser: req.body.produser,
              sutradara: req.body.sutradara,
              scooring: req.body.scooring,
              durasi: req.body.durasi,
            }
          : item
      );

      // It will response to client with status 201 (Created) and { data: data }
      res.status(200).json({ data: data });
    } catch (error) {
      // If something error, it will return response with status 500 and { errors: ["Internal Server Error"] }
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  deleteFilm(req, res, next) {
    try {
      // Find the data is exist or not
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      // If data not exists
      if (!findData) {
        // It will response client with status 404 and { errors: ["Film gak ketemu nih, belum release mungkin ya"] }
        return res
          .status(404)
          .json({ errors: ["Film gak ketemu nih, belum release mungkin ya"] });
      }

      // Delete data student to data by req.params.id
      data = data.filter((item) => item.id !== parseInt(req.params.id));

      // It will response to client with status 201 (Created) and { data: data }
      res.status(200).json({ data: data });
    } catch (error) {
      // If something error, it will return response with status 500 and { errors: ["Internal Server Error"] }
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }
}

// Exports the class
module.exports = new FilmMarvel();
