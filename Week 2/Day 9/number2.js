const eventAssigntment = require("../Day 10/eventAssigntment"); // Import index to run rl on this file

// Menampilkan Data Pasien
let patientName = [
  {
    name: "Syafa",
    status: "Positive",
  },
  {
    name: "Arsyad",
    status: "Positive",
  },
  {
    name: "Anggun",
    status: "Negative",
  },
  {
    name: "Raden",
    status: "Suspect",
  },
  {
    name: "Najib",
    status: "Negative",
  },
  {
    name: "Dhea",
    status: "Suspect",
  },
  {
    name: "Adi",
    status: "Positive",
  },
  {
    name: "Doni",
    status: "Suspect",
  },
  {
    name: "Agif",
    status: "Suspect",
  },
  {
    name: "Nanda",
    status: "Suspect",
  },
];

// Deklarasi Keadaan
let status = "Positive";

// Switch Case
switch (status) {
  case "Positive":
    const dataPositive = patientName.filter((el) => el.status === "Positive");
    console.log(dataPositive);
    break;
  case "Suspect":
    const dataSuspect = patientName.filter((el) => el.status === "Suspect");
    console.log(dataSuspect);
    break;
  case "Negative":
    const dataNegative = patientName.filter((el) => el.status === "Negative");
    console.log(dataNegative);
    break;
}
eventAssigntment.rl;

module.exports = [patientName];
