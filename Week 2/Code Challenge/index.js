const data = require("./lib/arrayFactory");
const test = require("./lib/test");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  const cleanNull = clean(data);

  for (let i = 1; i < cleanNull.length; i++) {
    for (let j = 0; j < cleanNull.length; j++) {
      if (cleanNull[i] < cleanNull[j]) {
        let a = cleanNull[i];
        cleanNull[i] = cleanNull[j];
        cleanNull[j] = a;
      }
    }
  }
  return cleanNull;
}

// Should return array
function sortDecending(data) {
  // Code Here
  const cleanNull = clean(data);

  for (let i = 1; i < cleanNull.length; i++) {
    for (let j = 0; j < cleanNull.length; j++) {
      if (cleanNull[i] > cleanNull[j]) {
        let a = cleanNull[i];
        cleanNull[i] = cleanNull[j];
        cleanNull[j] = a;
      }
    }
  }
  return cleanNull;
}
// DON'T CHANGE
test(sortAscending, sortDecending, data);
