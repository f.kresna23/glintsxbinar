const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate tube volume
function tube(phi, jari, height) {
  return phi * jari ** 2 * height;
}

// Function for inputing Phi to tube
function inputPhi() {
  rl.question(`Phi: `, (phi) => {
    if (!isNaN(phi)) {
      inputJari(phi);
    } else {
      console.log(`Phi should be 3.14\n`);
      inputPhi();
    }
  });
}

// Function for inputing jari to tube
function inputJari(phi) {
  rl.question(`Jari-jari:`, (jari) => {
    if (!isNaN(jari)) {
      inputHeight(phi, jari);
    } else {
      console.log(`You should enter ne number of Jari-jari\n`);
    }
  });
}

// Function for inputing height to tube
function inputHeight(phi, jari) {
  rl.question(`Height:`, (height) => {
    if (!isNaN(height)) {
      console.log(`\nTube: ${tube(phi, jari, height)}`);
      rl.close();
    } else {
      console.log(`You should enter ne number of Height\n`);
    }
  });
}

console.log(`Tube`);
console.log(`=========`);
inputPhi();
