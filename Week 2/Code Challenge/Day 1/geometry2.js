const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate prism volume
function prism(aOb, height) {
  return aOb * height;
}

function input() {
  rl.question("Area of Base: ", function (aOb) {
    rl.question("Height: ", (height) => {
      if (aOb > 0 && height > 0) {
        console.log(`\nPrism : ${prism(aOb, height)}`);
        rl.close();
      } else {
        console.log(`you should put the number to Area of Base and Height\n`);
        input();
      }
    });
  });
}

console.log(`Prism`);
console.log(`=========`);
input();
